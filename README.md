#jQuery easyScroll: A facebook style content scroller

 

###Syntax:

                

     
```
#!javascript

$(‘<divclass>’).easyScroll({option1:value, option2:value…. optionN:value});
```


 

###Usage:

1 - Create a div element that will possess an easyScroll plugin. 

```
#!html

<div class=”scrollable”></div>.
```

2 - Put required contents inside the div element.

3 - Define required scripts in html header (jquery.min.js, jquery-ui.min.js and easyscroll.js):

 

```
#!html

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="easyscroll.js"></script>

```

4 - declare .scrollable class as easyScroll.

 

```
#!javascript

$(function(){
    $('.scrollable').easyScroll({height:'400px',width:'200px'});
});

```

… and you’re done.


###Options:


Serial | Name             | Description                                  | Default Value  |
------:|------------------|----------------------------------------------|:---------------|
1      |width             | Width of the container div                   |‘auto’          |
2      |height            | Height of the container div                  |‘250px’         |
3      |railWidth         | Width of the scroll rail                     |‘10px’          |
4      |barRadius         | Radius of the scroll bar                     |‘7px’           |
5      |railRadius        | Radius of the scroll rail                    |‘7px’           |
6      |barColor          | Color of the scroll bar                      |‘#000’          |
7      |railColor         | Color of the scroll rail                     |‘#999’          |
8      |railPos           | Position of the scroll rail (left/right)     |‘right’         |
9      |posPadding        | Padding for the position of the rail         |‘0px’           |
10     |barOpacity        | Opacity of the scroll bar                    |.2              |
11     |railOpacity       | Opacity of the rail                          |.4              |
12     |verticalAlignment | Vertical Alignment of the rail (top/bottom)  |‘top’           |
13     |alwaysVisible     | Always visible (true/false)                  |true            |

 
###Methods:


`scrollToTop()` : Scrolls to the top of the content.

`scrollToBottom()`: Scrolls to the bottom of the content.

`resetBar()`: Redraws bar and is required when content had been changed with increased/decreased height.

 

###Example code:


```
#!html

<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="easyscroll.js"></script>

    <script>
        $(function(){
            $('.scrollable').easyScroll({height:'400px',width:'200px'});
            $('.scrollable').easyScroll('resetBar');
            $('.scrollable').easyScroll('scrollToBottom');
        });
    </script>

</head>

<body>
   <div class="scrollable">
      <ul>
          <li>Item content here first line</li>
          <li>Item content here 2</li>
          <li>Item content here 3</li>
                       .
                       .
                       .
          <li>Item content here N</li>
      <ul>
</body>


```
###Output: 
Please open Usage.htm or download the sample for a basic example.