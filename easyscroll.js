/**
 * jquery.easyScroll.js
 * @version: v1.13.4
 * @author: Ahmad Maroof Al Mamun
 *
 * Created by Ahmad Maroof Al Mamun on 2015-07-21.
 *
 * Copyright (c) 2015 Ahmad Maroof Al Mamun
 *
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

(function($) {
	
	// Constants
	var DIV_TAGS='<div></div>', MIN_BAR_HEIGHT=10, WHEEL_STEPS=30;
	//Option Variables
	var handler, sWidth, sHeight, rWidth, barRadius,	railRadius, barColor, railColor, railPos, posPadding, barOpacity, railOpacity, vAlign, alwaysVisible;
	// Activity Variables
	var isOverPanel, isOverBar, isDrag, isOverRail,isBarVisible,isBarWider, isPageSmaller, hidingQueue;
	var panel, bar, rail;
			
    var methods = {
        init : function(pluginOptions){init(pluginOptions);},
        scrollToTop : function() {  scrollTop();  },// IS
        scrollToBottom : function() { scrollBottom(); },// GOOD
        resetBar : function() { setBarHeight(); }// !!!
    };

    $.fn.easyScroll = function(methodOrOptions) {
        handler=this;
		if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.easyScroll' );
        }    
    };

	// ******************* Method definitions ******************************
	//Function: Scroll contents
	var scrollContent = function(scrollDirection,isWheel, animate)	{
		wheelStep=WHEEL_STEPS * scrollDirection;
		var scrollRatio = panel.outerHeight()/rail.outerHeight();

		if (isWheel)
		{
			var barCoeff=bar.position().top + wheelStep;
			if(barCoeff<0) barCoeff=0; 
			else if ((barCoeff + bar.outerHeight()) > rail.outerHeight()) barCoeff = rail.outerHeight() - bar.outerHeight();
			
			//Set bar on scroll
			bar.css('top', (barCoeff) +'px');
		} 
		//Set content position
		if (animate) panel.animate({top: -(Math.max(bar.position().top,0) * scrollRatio)+'px'}, { duration: 'fast', queue: false });
		else panel.css('top',-(Math.max(bar.position().top,0) * scrollRatio)+'px');
		
		
		if(!isDrag && !isOverRail && !isOverBar && isBarVisible) hideBar();
	}

	// Function: Scroll up to top
	var scrollTop = function(){
		if(!isPageSmaller) {
			bar.css('top', (rail.position().top)+'px');
			scrollContent(1,false,true);				
		}
		else panel.css('top', rail.outerHeight() + 'px');		
	}
	
	// Function: Scroll up to bottom
	var scrollBottom = function(){
		if(!isPageSmaller){
			bar.css('top', (rail.outerHeight() - bar.outerHeight())+'px');
			scrollContent(1,false,true);				
		}
		else {
			panel.css('top', (rail.outerHeight() - panel.outerHeight()) + 'px');
		}
	}
	
	// Function: Set height of the bar, proportionate to page height
	var setBarHeight = function()
	{
		//if page is not scrollable, return
		if(rail.outerHeight() >= panel.outerHeight()){
			isPageSmaller = true;
			return;
		}
		else isPageSmaller = false;
		
		var heightRatio=rail.outerHeight() / panel.outerHeight();
		var reqdBarHeight = Math.max(heightRatio * rail.outerHeight(), MIN_BAR_HEIGHT);
		bar.css({ height: reqdBarHeight + 'px' });
	}
				
    // Constructior
	var init = function(pluginOptions) {
			
            var opns = pluginOptions;
			$this=$(handler);
			//do it for every element that matches selector
			
			if(typeof pluginOptions === 'object')
			$this.each(function(){

                isOverPanel=false, isOverBar=false, isDrag=false, isOverRail=false,isBarVisible=false,isBarWider=false, hidingQueue;
                var o = opns || {},
                sWidth = o.width || 'auto',
                sHeight = o.height || '250px',
                rWidth = o.railWidth || '10px',
				barRadius = o.barRadius || '7px',
				railRadius = o.railRadius || '7px',
                barColor = o.barColor || '#000',
				railColor = o.railColor || '#999',
                railPos = o.railPos || 'right',
				posPadding = o.railPosPad || '0px',
				barOpacity = o.barOpaciy || .2,
                railOpacity = o.railOpacity || .4,
				vAlign	= o.verticalAlignment || 'top',
                alwaysVisible = o.alwaysVisible === true;
            
                //used in event handlers and for better minification
                panel = $($this);
                //wrap content
                var wrapperDiv = $(DIV_TAGS).css({
                    position: 'relative',
                    overflow: 'hidden',
                    width: sWidth,
                    height: sHeight
                }).attr({ 'class': 'easyScrollWrapper' });

                //update style for the div
                panel.css({
					position: 'absolute',
                    width: sWidth,
                    height: 'auto'
                });

                //create scrollbar rail
                rail  = $(DIV_TAGS).attr({ 
                    'class': 'easyScrollRail ', 
                    style: 'border-radius: ' + railRadius 
                    }).css({
                    width: rWidth,
                    height: '100%',
                    position: 'absolute',
					left: (railPos=='left') ? posPadding : 'auto',
					right: (railPos=='right') ? posPadding : 'auto',
					background: railColor,
					BorderRadius: railRadius,
					MozBorderRadius: railRadius,
					WebkitBorderRadius: railRadius,
					opacity: railOpacity,
					zIndex: 50
					
                });

                //create scrollbar
                bar  = $(DIV_TAGS).attr({ 
                    'class': 'easyScrollBar ', 
                    style: 'border-radius: ' + barRadius 
                    }).css({
                    width: rWidth,
                    height: '10px',
                    position: 'absolute',
					left: (railPos=='left') ? posPadding : 'auto',
					right: (railPos=='right') ? posPadding : 'auto',
					background: barColor,
					BorderRadius: barRadius,
					MozBorderRadius: barRadius,
					WebkitBorderRadius: railRadius,
					opacity: barOpacity,
					zIndex: 90
					
                });

				// Working Functions 
				var animateBar = function(isHover)
				{
					
					if(isHover){
						
						rail.animate({width: parseInt(rWidth)+2+'px'}, { duration: 'fast', queue: false });
						bar.animate({width: parseInt(rWidth)+2+'px'}, { duration: 'fast', queue: false });
						bar.css('left','auto');
						isBarWider=true;
					}
					else
					{
						rail.animate({width: rWidth}, { duration: 'fast', queue: false });
						bar.animate({width: rWidth}, { duration: 'fast', queue: false });
						// Opposite sides value may change on dragging
						(railPos == 'left') ? bar.css('left','auto') : bar.css('left','auto');
						isBarWider=false;
					}
				}
				
				panel.hover(function(){
					isOverPanel=true;	
					showBar();
					if(!isDrag){ 
						if(isBarVisible) hideBar();
						if(isBarWider) animateBar(false);
					}
 				},function(){
					isOverPanel=false;
					if(!isDrag && !isOverRail && !isOverBar && isBarVisible) hideBar();
				});
				
				rail.hover(function(){
					isOverRail=true;
					if(!isBarVisible)showBar();
					if(!isBarWider) animateBar(true);
				},function(){
					isOverRail=false;
					if(!isDrag && !isOverBar && isBarVisible) hideBar();
				});

				rail.click(function(e){
					if(isPageSmaller) return;
					var offsetY = e.pageY - parseInt(rail.offset().top);
					var newBarTop = offsetY - (bar.outerHeight() / 2);
					if(newBarTop < rail.position().top) newBarTop = rail.position().top;
					else if ((newBarTop + bar.outerHeight())  > rail.outerHeight()) newBarTop = rail.outerHeight() - bar.outerHeight();
					bar.css('top',(newBarTop) + 'px');
					scrollContent(1,false,true);
				});

				bar.hover(function(){
					
					isOverBar=true;
					if(!isBarVisible) showBar();
					if(!isBarWider) animateBar(true);
				},function(){
					isOverBar=false;
					if(!isDrag && !isOverRail && isBarVisible) hideBar();
				});
			
				function showBar()
				{
					if(isPageSmaller) return;
					isBarVisible=true;
					clearTimeout(hidingQueue);
					bar.fadeTo( 'fast', 0.6);
					rail.fadeTo('fast', 0.4);
					
				}
			
				function hideBar()
				{
					isBarVisible=false;
					clearTimeout(hidingQueue);
					// Hiding looses handler, so set alpha to zero.
					hidingQueue = setTimeout(function(){
						bar.fadeTo( 'slow', 0);
						rail.fadeTo('slow', 0);
					}, 1000);
					
				}

				var _onWheel = function(e)
				{
					//if page is not scrollable, return
					if(isPageSmaller) return;
					if(isOverPanel){
						if(!isBarVisible)showBar();
						if(isBarWider)animateBar(false);
						var e = e || window.event;
						//ie=wheelDelta, Mozilla=detail
						var delta = -(e.wheelDelta) || e.detail;
						scrollContent(delta/Math.abs(delta),true,false);
						//stop scroll
						if (e.preventDefault) { e.preventDefault(); }
						e.returnValue = false;
						
					}
				}
			
				var attachWheel = function()
				{
					if (window.addEventListener)
					{
						this.addEventListener('DOMMouseScroll', _onWheel, false );
						this.addEventListener('mousewheel', _onWheel, false );
					} 
					else
					{
						document.attachEvent("onmousewheel", _onWheel)
					}
				}

				bar.draggable({
					axis: 'y', 
					containment: 'parent',
					start: function() { isDrag = true; showBar();},
					stop: function() { isDrag = false; if(!isOverRail && !isOverBar && isBarVisible) hideBar(); },
					drag: function() { scrollContent(1,false,false)}
				});

								 
				
				var setupScroller = function(){	        
					//wrap it
					panel.wrap(wrapperDiv);
	
					//append to parent div
					panel.parent().append(rail);
					panel.parent().append(bar);
					
					//set initial height
					setBarHeight();
					hideBar();
					//attach scroll events
					attachWheel();
				}
				setupScroller();



            });

            //maintain chainability
            return this;
        }


})(jQuery);
